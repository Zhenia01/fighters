import { IHTMLElement } from "../interfaces";

export function createElement({
  tagName,
  className = "",
  attributes = {},
}: IHTMLElement) : HTMLElement {
  const element = document.createElement(tagName);

  if (className) {
    element.classList.add(className);
  }

  Object.keys(attributes).forEach(key =>
    element.setAttribute(key, (attributes as any)[key])
  );

  return element;
}
