export interface IHTMLElement {
  tagName: string;
  className?: string;
  attributes?: object;
}

export interface IFighter {
  _id: number;
  name: string;
  source: string;
}

export interface IFighterDetails extends IFighter {
  attack: number;
  defense: number;
  health: number;
}

export interface ITitledHTMLElement {
  title: string;
  body: HTMLElement;
}
