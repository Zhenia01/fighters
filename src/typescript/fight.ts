import { IFighterDetails, IFighter } from "./interfaces";

export function fight(
  firstFighter: IFighterDetails,
  secondFighter: IFighterDetails
): IFighter {
  let firstAttacks = true;

  const firstFighterCopy: IFighterDetails = Object.assign({}, firstFighter);
  const secondFighterCopy: IFighterDetails = Object.assign({}, secondFighter);

  while (firstFighterCopy.health > 0 && secondFighterCopy.health > 0) {
    if (firstAttacks) {
      const damage = getDamage(firstFighterCopy, secondFighterCopy);
      secondFighterCopy.health -= damage;
    } else {
      const damage = getDamage(secondFighterCopy, firstFighterCopy);
      firstFighterCopy.health -= damage;
    }
    firstAttacks = !firstAttacks;
  }
  return firstFighterCopy.health <= 0 ? secondFighterCopy : firstFighterCopy;
}

export function getDamage(
  attacker: IFighterDetails,
  enemy: IFighterDetails
): number {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter: IFighterDetails): number {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter: IFighterDetails): number {
  return fighter.defense * (Math.random() + 1);
}
