import { callApi } from '../helpers/apiHelper';

export async function getFighters(): Promise<any> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: number): Promise<any> {
  try {
    const endpoint = `/details/fighter/${id.toString()}.json`;
    return await callApi(endpoint, 'GET');
  } catch (error) {
    throw error;
  }
}

