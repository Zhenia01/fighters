import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IFighter, IFighterDetails } from './interfaces';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters: IFighter[]) : HTMLElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, (_ev, f) => showFighterDetails(f), selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

async function showFighterDetails(fighter: IFighter): Promise<void> {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: number): Promise<IFighterDetails> {
  return await getFighterDetails(fighterId);
}

function createFightersSelector() : (ev: Event, fighter: IFighter) => Promise<void> {
  const selectedFighters = new Map<number, IFighterDetails>();

  return async function selectFighterForBattle(event: Event, fighter: IFighter): Promise<void> {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((event.target as HTMLInputElement).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const values = selectedFighters.values();
      const winner: IFighter = fight(values.next().value, values.next().value);
      showWinnerModal(winner);
    }
  }
}
