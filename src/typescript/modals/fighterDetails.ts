import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";
import { IFighterDetails } from "../interfaces";

export function showFighterDetailsModal(fighter: IFighterDetails): void {
  const title = "Fighter info";
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, body: bodyElement });
}

function createFighterDetails(fighter: IFighterDetails): HTMLElement {
  const { name } = fighter;

  const fighterDetails = createElement({
    tagName: "div",
    className: "modal-body",
  });
  const nameElement = createElement({
    tagName: "span",
    className: "fighter-name",
  });
  nameElement.innerText = name;

  const attackElement = createElement({
    tagName: "p"
  });
  attackElement.innerText = `Attack: ${fighter.attack}`;

  const defenceElement = createElement({
    tagName: "p"
  });
  defenceElement.innerText = `Defence: ${fighter.defense}`;

  const healthElement = createElement({
    tagName: "p"
  });
  healthElement.innerText = `Health: ${fighter.health}`;

  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenceElement);
  fighterDetails.append(healthElement);

  return fighterDetails;
}
