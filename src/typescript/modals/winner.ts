import { IFighter } from "../interfaces";
import { showModal } from "./modal";
import { createElement } from "../helpers/domHelper";

export function showWinnerModal(fighter: IFighter): void {
  const title = "Winner";
  const body = createElement({
    tagName: "p",
    attributes: {"style": "text-align:center; font-size: 2rem;"}
  });
  body.innerText = `${fighter.name} has won!`;
  showModal({ title, body });
}
