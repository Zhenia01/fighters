"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apiHelper_1 = require("../helpers/apiHelper");
async function getFighters() {
    try {
        const endpoint = 'fighters.json';
        const apiResult = await apiHelper_1.callApi(endpoint, 'GET');
        return apiResult;
    }
    catch (error) {
        throw error;
    }
}
exports.getFighters = getFighters;
async function getFighterDetails(id) {
    try {
        const endpoint = `/details/fighter/${id.toString()}.json`;
        return await apiHelper_1.callApi(endpoint, 'GET');
    }
    catch (error) {
        throw error;
    }
}
exports.getFighterDetails = getFighterDetails;
