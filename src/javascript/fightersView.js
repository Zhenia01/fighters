"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fighterView_1 = require("./fighterView");
const fighterDetails_1 = require("./modals/fighterDetails");
const domHelper_1 = require("./helpers/domHelper");
const fight_1 = require("./fight");
const winner_1 = require("./modals/winner");
const fightersService_1 = require("./services/fightersService");
function createFighters(fighters) {
    const selectFighterForBattle = createFightersSelector();
    const fighterElements = fighters.map(fighter => fighterView_1.createFighter(fighter, (_ev, f) => showFighterDetails(f), selectFighterForBattle));
    const fightersContainer = domHelper_1.createElement({ tagName: 'div', className: 'fighters' });
    fightersContainer.append(...fighterElements);
    return fightersContainer;
}
exports.createFighters = createFighters;
async function showFighterDetails(fighter) {
    const fullInfo = await getFighterInfo(fighter._id);
    fighterDetails_1.showFighterDetailsModal(fullInfo);
}
async function getFighterInfo(fighterId) {
    return await fightersService_1.getFighterDetails(fighterId);
}
exports.getFighterInfo = getFighterInfo;
function createFightersSelector() {
    const selectedFighters = new Map();
    return async function selectFighterForBattle(event, fighter) {
        const fullInfo = await getFighterInfo(fighter._id);
        if (event.target.checked) {
            selectedFighters.set(fighter._id, fullInfo);
        }
        else {
            selectedFighters.delete(fighter._id);
        }
        if (selectedFighters.size === 2) {
            const values = selectedFighters.values();
            const winner = fight_1.fight(values.next().value, values.next().value);
            winner_1.showWinnerModal(winner);
        }
    };
}
