"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domHelper_1 = require("../helpers/domHelper");
function showModal(element) {
    const root = getModalContainer();
    const modal = createModal(element);
    root.append(modal);
}
exports.showModal = showModal;
function getModalContainer() {
    return document.getElementById("root");
}
function createModal({ title, body }) {
    const layer = domHelper_1.createElement({ tagName: "div", className: "modal-layer" });
    const modalContainer = domHelper_1.createElement({
        tagName: "div",
        className: "modal-root",
    });
    const header = createHeader(title);
    modalContainer.append(header, body);
    layer.append(modalContainer);
    return layer;
}
function createHeader(title) {
    const headerElement = domHelper_1.createElement({
        tagName: "div",
        className: "modal-header",
    });
    const titleElement = domHelper_1.createElement({ tagName: "span" });
    const closeButton = domHelper_1.createElement({ tagName: "div", className: "close-btn" });
    titleElement.innerText = title;
    closeButton.innerText = "×";
    closeButton.addEventListener("click", hideModal);
    headerElement.append(title, closeButton);
    return headerElement;
}
function hideModal() {
    var _a;
    const modal = document.getElementsByClassName("modal-layer")[0];
    (_a = modal) === null || _a === void 0 ? void 0 : _a.remove();
}
