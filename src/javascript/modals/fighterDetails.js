"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domHelper_1 = require("../helpers/domHelper");
const modal_1 = require("./modal");
function showFighterDetailsModal(fighter) {
    const title = "Fighter info";
    const bodyElement = createFighterDetails(fighter);
    modal_1.showModal({ title, body: bodyElement });
}
exports.showFighterDetailsModal = showFighterDetailsModal;
function createFighterDetails(fighter) {
    const { name } = fighter;
    const fighterDetails = domHelper_1.createElement({
        tagName: "div",
        className: "modal-body",
    });
    const nameElement = domHelper_1.createElement({
        tagName: "span",
        className: "fighter-name",
    });
    nameElement.innerText = name;
    const attackElement = domHelper_1.createElement({
        tagName: "p"
    });
    attackElement.innerText = `Attack: ${fighter.attack}`;
    const defenceElement = domHelper_1.createElement({
        tagName: "p"
    });
    defenceElement.innerText = `Defence: ${fighter.defense}`;
    const healthElement = domHelper_1.createElement({
        tagName: "p"
    });
    healthElement.innerText = `Health: ${fighter.health}`;
    fighterDetails.append(nameElement);
    fighterDetails.append(attackElement);
    fighterDetails.append(defenceElement);
    fighterDetails.append(healthElement);
    return fighterDetails;
}
