"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const modal_1 = require("./modal");
const domHelper_1 = require("../helpers/domHelper");
function showWinnerModal(fighter) {
    const title = "Winner";
    const body = domHelper_1.createElement({
        tagName: "p",
        attributes: { "style": "text-align:center; font-size: 2rem;" }
    });
    body.innerText = `${fighter.name} has won!`;
    modal_1.showModal({ title, body });
}
exports.showWinnerModal = showWinnerModal;
