"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function fight(firstFighter, secondFighter) {
    let firstAttacks = true;
    const firstFighterCopy = Object.assign({}, firstFighter);
    const secondFighterCopy = Object.assign({}, secondFighter);
    while (firstFighterCopy.health > 0 && secondFighterCopy.health > 0) {
        if (firstAttacks) {
            const damage = getDamage(firstFighterCopy, secondFighterCopy);
            secondFighterCopy.health -= damage;
        }
        else {
            const damage = getDamage(secondFighterCopy, firstFighterCopy);
            firstFighterCopy.health -= damage;
        }
        firstAttacks = !firstAttacks;
    }
    return firstFighterCopy.health <= 0 ? secondFighterCopy : firstFighterCopy;
}
exports.fight = fight;
function getDamage(attacker, enemy) {
    const damage = getHitPower(attacker) - getBlockPower(enemy);
    return damage > 0 ? damage : 0;
}
exports.getDamage = getDamage;
function getHitPower(fighter) {
    return fighter.attack * (Math.random() + 1);
}
exports.getHitPower = getHitPower;
function getBlockPower(fighter) {
    return fighter.defense * (Math.random() + 1);
}
exports.getBlockPower = getBlockPower;
